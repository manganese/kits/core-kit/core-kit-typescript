enum OrderByDirection {
  ASCENDING = 'asc',
  DESCENDING = 'desc'
}


export default OrderByDirection;