enum VersionType {
  MAJOR = 3,
  MINOR = 2,
  PATCH = 1
}


export default VersionType;