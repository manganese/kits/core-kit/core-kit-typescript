enum HttpMethod {
  OPTIONS = 'OPTIONS',
  GET     = 'GET',
  PATCH   = 'PATCH',
  POST    = 'POST',
  PUT     = 'PUT',
  DELETE  = 'DELETE',
}


export default HttpMethod;