enum NetworkState {
  CONNECTING   = 'connecting',
  CONNECTED    = 'connected',
  REFUSED      = 'refused',
  TIMED_OUT    = 'timed-out',
  UNRESOLVABLE = 'unresolvable'
}


export default NetworkState;