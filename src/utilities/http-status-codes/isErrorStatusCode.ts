import { HttpStatusCode } from "../../enumerations";

export default (statusCode: HttpStatusCode) => {
  return statusCode && statusCode >= 300;
}
