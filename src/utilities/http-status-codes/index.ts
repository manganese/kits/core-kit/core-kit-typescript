import isSuccessStatusCode from './isSuccessStatusCode';
import isErrorStatusCode from './isErrorStatusCode';
import isClientErrorStatusCode from './isClientErrorStatusCode';
import isServerErrorStatusCode from './isServerErrorStatusCode';

export {
  isSuccessStatusCode,
  isErrorStatusCode,
  isClientErrorStatusCode,
  isServerErrorStatusCode
}
