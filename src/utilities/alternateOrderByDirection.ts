// Enumerations
import OrderByDirection from "../enumerations/OrderByDirection";


export default (direction: OrderByDirection): OrderByDirection => {
  switch (direction) {
    case OrderByDirection.ASCENDING:  return OrderByDirection.DESCENDING;
    case OrderByDirection.DESCENDING: return OrderByDirection.ASCENDING;
  }
};