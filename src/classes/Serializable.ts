export default abstract class Serializable {
  abstract serialize(): object

  static deserialize(data: object): any {
    throw new Error("No implementation for deserialize was provided by the extending class!");
  }
};