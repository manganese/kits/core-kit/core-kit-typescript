export default interface ScreenPosition {
  x: number,
  y: number
};