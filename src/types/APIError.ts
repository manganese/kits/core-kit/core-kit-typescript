// Enumerations
import HttpStatusCode from "../enumerations/HttpStatusCode";


export default interface APIError {
  type: string,
  message?: string,
  status: HttpStatusCode
};