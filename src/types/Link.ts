export default interface Link {
  method: string,
  uri: string
};