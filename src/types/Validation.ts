export default interface Validation {
  // Used to differentiate between different types of errors of a single field
  key?: string,

  success: boolean,
  type?: string,
  message?: string
};